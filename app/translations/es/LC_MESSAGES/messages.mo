��    ;      �              �     �     �     �               &     F     K     ^     p     �     �     �  !   �  &   �     �     �               #     4     9     V     c     i  	   p     z     �     �  "   �  &   �     �  *        ,     4     =     I     Y     p     v     �     �     �     �  A   �  	   �                    -     6     K  "   j     �     �     �     �     �  �       �	     �	     �	     �	     �	     

     (
  #   /
  $   S
     x
     �
     �
     �
      �
  ,   �
     �
               !     @     S  *   Z     �     �     �     �     �     �     �  .   �          6     Q     m     t  
   �     �     �     �     �     �     �     �     �  D   �  
   C     N     ^  .   f     �     �     �  !   �  "   �     "      A  !   b     �    Newer posts %(count)d followers %(count)d following %(username)s said %(when)s About me An unexpected error has ocurred Back Click to Register! Click to Reset It Congratulations! Edit Profile Edit your profile Email Error: Could not contact server.  Error: the translation service failed. Explore File not found Follow Forgot Your Password? Hi,%(username)s! Home Invalid username or password Last seen on Login Logout New User? Newer posts Older posts Password Please log in to access this page. Please use a different email address.  Please use a different name Please use a different name. Already taken Profile Register Remember Me Repeat Password Request Password Reset Reset Reset Password Reset Your Password Say something Sign In Submit The administrator has been notified. Sorry for the inconvenience! Translate Unfollow User User %(username)s not found. Username Welcome to Microblog You are following %(username)s You are not following %(username)s You cannot follow yourself! Your change have been saved. Your password has been reset. Your post is now live! [Microblog] Reset Your Password Project-Id-Version: PROJECT VERSION
Report-Msgid-Bugs-To: EMAIL@ADDRESS
POT-Creation-Date: 2019-08-18 14:58-0700
PO-Revision-Date: 2017-09-29 23:25-0700
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language: es
Language-Team: es <LL@li.org>
Plural-Forms: nplurals=2; plural=(n != 1)
MIME-Version: 1.0
Content-Type: text/plain; charset=utf-8
Content-Transfer-Encoding: 8bit
Generated-By: Babel 2.7.0
 Posts nuevos %(count)d seguidores siguiendo a %(count)d %(username)s dijo %(when)s Acerca de mí Un error inesperado ha pasado Atrás ¡Haz click aquí para registrarte! Haz click aquí para pedir una nueva Felicitaciones! Editar Perfil Editar tu perfil Email No se pudo contactar el servidor Error: El servicio de traduccion ha fallado. Explorar Archivo no encontrado Seguir ¿Te olvidaste tu contraseña? Hola, %(username)s Inicio Nombre de usuario o contraseña inválidos Última visita Ingresar Salir ¿Usuario Nuevo? Artículos siguientes Artículos previos Contraseña Por favor ingrese para acceder a esta página. Use una direccion diferente.  Use un nombre diferente.   Porfavor, use otro nombre.  Perfil Registrarse Recordarme Repetir Contraseña Pedir una nueva contraseña Reset Nueva Contraseña Nueva Contraseña Dí algo Ingresar Enviar El administrador ha sido notificado. ¡Lamentamos la inconveniencia! traduccion Dejar de seguir Usuario El usuario %(username)s no ha sido encontrado. Nombre de usuario Bienvenido a Microblog Estas siguiendo a %(username)s No estas siguiendo a %(username)s ¡No te puedes seguir a tí mismo! Sus cambios han sido guardados Tu contraseña ha sido cambiada. ¡Tu artículo ha sido publicado! [Microblog] Nueva Contraseña 